<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "postings".
 *
 * @property integer $id
 * @property string $content
 * @property string $date
 * @property string $name
 */
class Postings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'content', 'name'], 'required'],
            [['id'], 'integer'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'date' => 'Date',
            'name' => 'Name',
        ];
    }
}
