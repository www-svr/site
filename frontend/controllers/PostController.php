<?php

namespace frontend\controllers;

use app\models\Postings;

class PostController extends \yii\web\Controller
{
    public function actionGet($name)
    {   
        $dataProvider=Postings::find()->where(['name'=>$name])->one();
        return $this->render('get',['dataProvider'=>$dataProvider]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView()
    {
        return $this->render('view');
    }

}
