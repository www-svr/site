<?php

use yii\helpers\Html;
use yii\bootstrap\Button;
?>
<div class="highlight col-sm-9">
	
	<div class="row">
			<div class="col-sm-3 col-xs-3"><?= Html::img('http://placehold.it/150x150') ?>
			<h6><?= date($data) ?></h6></div>
            <div class="col-xs-9 "><h1><?= Html::encode($name) ?></h1> <?= Html::decode($text) ?> 
		            <div class='col-xs-2 col-xs-offset-9'> <?= Button::widget([
					    'label' => 'Action',
					    'options' => ['class' => 'btn-lg '],
					]); ?></div>
			</div>
            
    </div>
    
</div>