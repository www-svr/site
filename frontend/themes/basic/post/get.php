<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use app\models\Postings;

$this->title = $dataProvider->name;

$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= $this->title ?></h1>

<p>
	<?= $dataProvider->content ?>
    
</p>
