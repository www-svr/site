<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Collapse;
use yii\widgets\Breadcrumbs;
use frontend\assets\KtelAsset;
use frontend\widgets\Alert;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

KtelAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
                
                        
        <?php
            NavBar::begin([
                'brandLabel' => 'KNet',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                
                ['label' => 'ru', 'url' => ['/site/index']],
                ['label' => 'ua', 'url' => ['/site/index']],
                
            ];
            if (Yii::$app->user->isGuest) {
                
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right '],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="row container" >
        <div class="col-md-2">
        <table>
                    <tbody><tr>
                            <td valign="top"><center><img src="/images/phone.png"></center></td>
                            <td>
                                <h6>Киев</h6>
                                <h3>(044) 370-81-96</h3>
                                <h6>Обухов / Украинка</h6>
                                <h4>(04572) 68-140</h4>
                            </td>
                        </tr>
                        <tr>
                            <td><center><img src="/images/skype.png"></center></td>
                            <td><h5>k-abonent</h5></td>
                        </tr>
                        <tr>
                            <td><center><img src="/images/email.png"></center></td>
                            <td><h5>support@ktelecom.in.ua</h5></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="#" data-reveal-id="myModal">Есть вопрос?</a></td>
                        </tr>
                    </tbody>
                </table>

        <?php
            $menuItemsInter = [
                ['label' => 'Payments', 'url' => ['/post/get','name'=>'premium']],
                ['label' => 'Payments', 'url' => ['/site/index2']],
                ['label' => 'Payments', 'url' => ['/site/index3']],
                ['label' => 'Payments', 'url' => ['/site/index4']],
                ['label' => 'Payments', 'url' => ['/site/index4']],
                ['label' => 'Payments', 'url' => ['/site/index5']],
                
            ];
            $menuItemsTel = [
                ['label' => 'Payments', 'url' => ['/site/index1']],
                ['label' => 'Payments', 'url' => ['/site/index2']],
                               
            ];
            $menuItemsIPTV = [
                ['label' => 'Payments', 'url' => ['/site/index1']],
                ['label' => 'Payments', 'url' => ['/site/index2']],
                               
            ];

            $menuItemsAdd = [
                ['label' => 'Payments', 'url' => ['/site/index1']],
                ['label' => 'Payments', 'url' => ['/site/index2']],
                               
            ];
            echo Collapse::widget([

                'items' => [
                    [
                        'label' => 'Payments',
                        'content' => Nav::widget([
                                'options' => ['class' => 'nav nav-pills nav-stacked'],
                                'items' => [['label'=>'Payments','url'=>['/site/ind']]],
                            ]),
                        
                    ],
                    // equivalent to the above
                    [
                        'label' => 'Internet',
                        'content' => Nav::widget([
                                'options' => ['class' => 'nav nav-pills nav-stacked'],
                                'items' => $menuItemsInter,
                            ]),
                        // open its content by default
                        'contentOptions' => ['class' => 'in']
                    ],
                    // another group item
                    [
                        'label' => 'Telephone',
                        'content' => Nav::widget([
                                'options' => ['class' => 'nav nav-pills nav-stacked'],
                                'items' => $menuItemsTel,
                            ]),
                        
                    ],
                    [
                        'label' => 'IPTV',
                        'content' => Nav::widget([
                                'options' => ['class' => 'nav nav-pills nav-stacked'],
                                'items' => $menuItemsIPTV,
                            ]),
                        
                    ],
                    [
                        'label' => 'CCTV',
                        'content' => Nav::widget([
                                'options' => ['class' => 'nav nav-pills nav-stacked'],
                                'items' => [['label'=>'Payments','url'=>['/site/ind']]],
                            ]),
                        
                    ],
                    [
                        'label' => 'Additional',
                        'content' => Nav::widget([
                                'options' => ['class' => 'nav nav-pills nav-stacked'],
                                'items' => $menuItemsAdd,
                            ]),
                        
                    ],


                ]
            ]);
             
        ?>

        </div>
        <div class="content col-md-10">
        <?php 
        if (\Yii::$app->requestedRoute!='site/index') {
            echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        }
         ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        
            <div class="box">
            <div id="fleft">
                <table>
                    <tbody><tr>
                        <td><center><img src="/images/skype.png"></center></td>
                        <td><h5>k-abonent</h5></td>
                        <td rowspan="2"><center><img src="/images/address.png"></center></td>
                        <td><h5>г. Киев</h5></td>
                    </tr>
                    <tr>
                        <td><center><img src="/images/email.png"></center></td>
                        <td><h5>support@ktelecom.in.ua</h5></td>
                        <td><h5>ул. Саксаганского 67б</h5></td>
                    </tr>   
                </tbody></table>
            </div>
            <div id="fright">
                <table>
                    <tbody><tr>
                        <td>
                            <h6>Киев</h6>
                            <h3>(044) 370-81-96</h3>
                        </td>
                        <td>
                            <h6>Обухов / Украинка</h6>
                            <h4>(04572) 68-140</h4>
                        </td>
                        <td><center><img src="/images/phone.png"></center></td>
                    </tr>
                </tbody></table>
            </div>
            </div>
        
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
