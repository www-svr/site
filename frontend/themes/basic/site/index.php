<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
use yii\bootstrap\Carousel;
use yii\helpers\Url;
use frontend\widgets\NewsFeed;

?>
<div class="site-index">
    
    <?= Carousel::widget([
    'items' => [
        // the item contains only the image
        '<img src="'.Url::to('images/picasso-girl-before-a-mirror.jpeg').'"/>',
        ['content' => '<img src="'.Url::to('images/picasso-girl-before-a-mirror.jpeg').'"/>'],
        // the item contains both the image and the caption
        [
            'content' => '<img src="'.Url::to('images/picasso-girl-before-a-mirror.jpeg').'"/>',
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            
    ]
]]); ?>

    <div class="body-content">

        <?= NewsFeed::widget() ?>

    </div>
</div>
