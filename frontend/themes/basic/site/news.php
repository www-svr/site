<?php

use yii\helpers\Html;
use app\models\News;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'News';

$this->params['breadcrumbs'][] = $this->title;
?>

<?php foreach ($dataProvider as $post ) {

	echo $this->render("@app/views/news/_view",$post->attributes);
}

	
?>
