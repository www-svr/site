<html>
	<head>
		<link rel="stylesheet" href="/css/style.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<meta charset="UTF-8">
		
		
		<link rel="stylesheet" href="/reval/reveal.css">
		<script src="/reval/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script src="/reval/jquery.reveal.js" type="text/javascript"></script>
		
	</head>

	<body>
		<div id="header">
			<div class="box">
			<div id="logo"><a href="/"><h1>K-<span>Net</span></h1></a></div>
			<div id="language">
				<ul>
					<li class="pasive"><a href="#">ru</a></li>
					<li class="active"><a href="#">ua</a></li>
				</ul>
			</div>
			<div id="billingin">
				<div id="bf">
					<div id="bftxt"><p>Вход<br>для пользователей</p></div>
					<div id="bfimg"><a href="/"><img src="/images/key.png"></a></div>
					<div id="bfin">
						<form name="input" action="/billing/" method="post">
							<input type="text" name="login" value="Логин">
							<input type="password" name="passwd" value="Пароль">
							<input class="submit" type="submit" value="Вход">
						</form> 
					</div>			
				</div>	
			</div>
			</div>
		</div>
		<div class="box">
			<div id="left">
				<div id="contacts">
					<table>
						<tr>
							<td valign="top"><center><img src="/images/phone.png"></center></td>
							<td>
								<h6>Киев</h6>
								<h3>(044) 370-81-96</h3>
								<h6>Обухов / Украинка</h6>
								<h4>(04572) 68-140</h4>
							</td>
						</tr>
						<tr>
							<td><center><img src="/images/skype.png"></center></td>
							<td><h5>k-abonent</h5></td>
						</tr>
						<tr>
							<td><center><img src="/images/email.png"></center></td>
							<td><h5>support@ktelecom.in.ua</h5></td>
						</tr>
						<tr>
							<td colspan="2"><a href="#"  data-reveal-id="myModal">Есть вопрос?</a></td>
						</tr>
					</table>
				</div>
				<div id="menubox">
					<div id="degree">
						<div class="forhome">
							<a class="forhomea" href="#">Для дома</a>
							<ul class="level1">
								<li><a href="#">Оплата услуг</a></li>
								<li>
									<a href="#">Интернет</a>
									<ul class="level2">
										<li><a href="#">Турбо</a></li>
										<li><a href="#">Быстрый</a></li>
										<li><a href="#">Удобный</a></li>
										<li><a href="#">Оптимальный</a></li>
										<li><a href="#">Базовый</a></li>
										<li><a href="#">Эконом</a></li>
										<li><a href="#">Карта покрытия</a></li>
									</ul>
								</li>
								<li>
									<a href="#">Телефония</a>
									<ul class="level2">
										<li><a href="#">Стационарный телефон</a></li>
										<li><a href="#">IP телефония</a></li>
									</ul>
								</li>
								<li>
									<a href="#">IP TV</a>
									<ul class="level2">
										<li><a href="#">Максимальный</a></li>
										<li><a href="#">Оптимальный</a></li>
										<li><a href="#">Базовый</a></li>
									</ul>
								</li>
								<li><a href="#">Видеонаблюдение</a></li>
								<li>
									<a href="#">Дополнительно</a>
									<ul class="level2">
										<li><a href="#">Выделеный ip-адресс</a></li>
										<li><a href="#">Антивирусная защита</a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="foroffice"><a class="forofficea" href="#">Для офиса</a></div>
					</div>
				</div>
				<div id="mapbox"></div>
			</div>		
			<div id="main">
				<div id="slider">
					<a href="#"><img src="/images/money.png"></a><a href="#"><h3><b>Онлайн оплата</b> позволит Вам   пользоваться нашими услугами более удобно и практично.</h3></a>
					<ul>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
					</ul>
				</div>
				<div id="tariff">
						<div class="tbox">
							<div class="theader"><h2>Турбо</h2></div>
							<div class="tcircle">
								<h3>100</h3>
								<h5>Мбит/с</h5>
							</div>
							<div class="tcont">
								<table>
									<tr class="tprice" >
										<td><p>Цена:</p></td><td><p><b>145</b> грн</p></td>
									</tr>
									<tr class="tbonus">
										<td><p>Бонус:</p></td><td><p><b>&nbsp;145 &nbsp;</b> грн</p></td>
									</tr>
								</table>
								<p><a href="#">Заказать</a></p>
							</div>
						</div>
						<div class="tbox active">
							<div class="theader"><h2>Быстрый</h2></div>
							<div class="tcircle">
								<h3>50</h3>
								<h5>Мбит/с</h5>
							</div>
							<div class="tcont">
								<table>
									<tr class="tprice" >
										<td><p>Цена:</p></td><td><p><b>85</b> грн</p></td>
									</tr>
									<tr class="tbonus">
										<td><p>Бонус:</p></td><td><p><b>&nbsp;85 &nbsp;</b> грн</p></td>
									</tr>
								</table>
								<p><a href="#">Заказать</a></p>
							</div>
						</div>
						<div class="tbox">
							<div class="theader"><h2>Удобный</h2></div>
							<div class="tcircle">
								<h3>25</h3>
								<h5>Мбит/с</h5>
							</div>
							<div class="tcont">
								<table>
									<tr class="tprice" >
										<td><p>Цена:</p></td><td><p><b>65</b> грн</p></td>
									</tr>
									<tr class="tbonus">
										<td><p>Бонус:</p></td><td><p><b>&nbsp;65 &nbsp;</b> грн</p></td>
									</tr>
								</table>
								<p><a href="#">Заказать</a></p>
							</div>
						</div>
						<div class="tbox">
							<div class="theader"><h2>Оптимальный</h2></div>
							<div class="tcircle">
								<h3>10</h3>
								<h5>Мбит/с</h5>
							</div>
							<div class="tcont">
								<table>
									<tr class="tprice" >
										<td><p>Цена:</p></td><td><p><b>55</b> грн</p></td>
									</tr>
									<tr class="tbonus">
										<td><p>Бонус:</p></td><td><p><b>&nbsp;55 &nbsp;</b> грн</p></td>
									</tr>
								</table>
								<p><a href="#">Заказать</a></p>
							</div>
						</div>
						<div class="tbox">
							<div class="theader"><h2>Базовый</h2></div>
							<div class="tcircle">
								<h3>5</h3>
								<h5>Мбит/с</h5>
							</div>
							<div class="tcont">
								<table>
									<tr class="tprice" >
										<td><p>Цена:</p></td><td><p><b>45</b> грн</p></td>
									</tr>
									<tr class="tbonus">
										<td><p>Бонус:</p></td><td><p><b>&nbsp;45 &nbsp;</b> грн</p></td>
									</tr>
								</table>
								<p><a href="#">Заказать</a></p>
							</div>
						</div>
						<div class="tbox">
							<div class="theader"><h2>Эконом</h2></div>
							<div class="tcircle">
								<h3>2</h3>
								<h5>Мбит/с</h5>
							</div>
							<div class="tcont">
								<table>
									<tr class="tprice" >
										<td><p>Цена:</p></td><td><p><b>35</b> грн</p></td>
									</tr>
									<tr class="tbonus">
										<td><p>Бонус:</p></td><td><p><b>&nbsp;35 &nbsp;</b> грн</p></td>
									</tr>
								</table>
								<p><a href="#">Заказать</a></p>
							</div>
						</div>
				</div>
				<div id="news">
					<div class="nbox">
						<div class="nimg"><a href="/"><img src="/images/nimg.png"></a></div>
						<div class="ntxt">
							<h3><a href="/">Каналы Viasat теперь и в «Базовом+»</a></h3>
							<p class="introtext">С 1 сентября 2014 года ТВ-пакет «Базовый+» пополнится тремя интереснейшими каналами группы Viasat. В дополнение каналу <b>TV 1000</b>, который ранее уже был представлен в сетке вещания, добавятся <b>Viasat Explore, Viasat Nature, Viasat Sport</b>.</p>
							<p><a class="readmore" href="/">Читать далее</a></p> 
						</div>
					</div>
					<div class="nbox">
						<div class="nimg"><a href="/"><img src="/images/nimg.png"></a></div>
						<div class="ntxt">
							<h3><a href="/">Изменения в сетке вещания</a></h3>
							<p class="introtext">С 1 сентября 2014 года Сеть Ланет в пакетах кабельного ТВ <b>«Базовый+»</b> и <b>«Базовый»</b> прекращает трансляцию каналов группы <b>Discovery</b>: Discovery Ukraine, Investigation Discovery, Discovery Science, Animal Planet, Discovery World, TLC.</p>
							<p><a class="readmore" href="/">Читать далее</a></p> 
						</div>
					</div>
					<div class="nbox">
						<div class="nimg"><a href="/"><img src="/images/nimg.png"></a></div>
						<div class="ntxt">
							<h3><a href="/">Снято кодирование Цифрового ТВ на праздники!</a></h3>
							<p class="introtext">Сеть Ланет снимает кодирование цифрового телевидения с 22 по 25 августа!</br>Поздравляем всех пользователей с наступающим Днем Независимости Украины.</p>
							<p><a class="readmore" href="/">Читать далее</a></p> 
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div id="footer">
			<div class="box">
			<div id="fleft">
				<table>
					<tr>
						<td><center><img src="/images/skype.png"></center></td>
						<td><h5>k-abonent</h5></td>
						<td rowspan="2"><center><img src="/images/address.png"></center></td>
						<td><h5>г. Киев</h5></td>
					</tr>
					<tr>
						<td><center><img src="/images/email.png"></center></td>
						<td><h5>support@ktelecom.in.ua</h5></td>
						<td><h5>ул. Саксаганского 67б</h5></td>
					</tr>	
				</table>
			</div>
			<div id="fright">
				<table>
					<tr>
						<td>
							<h6>Киев</h6>
							<h3>(044) 370-81-96</h3>
						</td>
						<td>
							<h6>Обухов / Украинка</h6>
							<h4>(04572) 68-140</h4>
						</td>
						<td><center><img src="/images/phone.png"></center></td>
					</tr>
				</table>
			</div>
			</div>
		</div>
		
		<div id="myModal" class="reveal-modal">
			<div id="clallback" class="wide dark">
				<div id="question" >
					<h1>Задать вопрос</h1>
					<form name="input" action="/" method="post">
							<p>Ф.И.О.</p> 
							<input type="text" name="Name" value=" ">
							<p>Email</p> 
							<input type="text" name="Email" value=" ">
							<p>Телефон</p> 
							<input type="text" name="Phone" value=" ">
							<p>Вопрос</p> 
							<textarea rows="4" cols="20" name="Message"></textarea>
							<input class="submit" type="submit" value="Отправить">
					</form> 
				</div>
				<div id="cbimg"><img src="/images/helpdep.png"></div>
				<div id="helpdep" >
					<h1>Абонентский отдел</h1>
					<table>
						<tr>
							<td valign="top"><center><img src="/images/phonew.png"></center></td>
							<td>
								<h6>Киев</h6>
								<h3>(044) 370-81-96</h3>
								<h6>Обухов / Украинка</h6>
								<h4>(04572) 68-140</h4>
							</td>
						</tr>
						<tr>
							<td><center><img src="/images/skypew.png"></center></td>
							<td><h5>k-abonent</h5></td>
						</tr>
						<tr>
							<td><center><img src="/images/emailw.png"></center></td>
							<td><h5>support@ktelecom.in.ua</h5></td>
						</tr>
					</table>
					<p>Сотрудники нашей компании всегда готовы помочь в решении ваших проблем ответить на все возникающее вопросы.  Вы можете связаться с нами любым удобным для вас способом или заполнить форму обратной связи.</p>
				</div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
	</body>
</html>