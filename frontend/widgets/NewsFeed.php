<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets;

use yii\data\ActiveDataProvider;
use app\models\News;

class NewsFeed extends \yii\bootstrap\Widget
{
    
    

    public function init()
    {
        parent::init();

        
        
    }
    public function run() 
    {

        $dataProvider=News::findAll(['state'=>1]);

        return $this->render('@app/views/site/news',['dataProvider' => $dataProvider,]);

    }
}
